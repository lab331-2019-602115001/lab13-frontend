import { TestBed } from '@angular/core/testing';

import { JwtIterceptorService } from './jwt-iterceptor.service';

describe('JwtIterceptorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: JwtIterceptorService = TestBed.get(JwtIterceptorService);
    expect(service).toBeTruthy();
  });
});
