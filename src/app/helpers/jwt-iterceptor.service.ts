import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class JwtIterceptorService implements HttpInterceptor{

  constructor() { }

  intercept(request: HttpRequest<any>, next: HttpHandler):
  Observable<HttpEvent<any>> {
    // add authorization header with jwt token if avaiable
    const token = localStorage.getItem('token');
    if (token) {
      request = request.clone({
        setHeaders: {
          authorization: 'Bearer ${token}'
        }
      });
    }

    return next.handle(request);
  }
}
